package com.example.venkateshk.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by venkateshk on 24/4/17.
 */
public class ConnectFragment extends Fragment {
    WebView webView;
    WebView wv;
    public ConnectFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_connect, container, false);


        wv=(WebView)rootView.findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("http://192.168.91.126:5601/app/kibana#/dashboard/922c6fe0-20d8-11e7-bcb9-b5b0748593aa?embed=true&_g=()&_a=(filters:!(),options:(darkTheme:!f),panels:!((col:7,id:f4c8c160-201c-11e7-a6d5-5b7a2c156bd0,panelIndex:1,row:1,size_x:6,size_y:7,type:visualization),(col:1,id:%271b847d80-2036-11e7-b54e-796ca9a883ed%27,panelIndex:2,row:1,size_x:6,size_y:3,type:visualization),(col:1,id:%277d4a44f0-1f55-11e7-965e-75c76c343bf1%27,panelIndex:3,row:4,size_x:6,size_y:3,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:%27*%27)),title:multiple,uiState:(P-1:(spy:(mode:(fill:!f,name:!n)),vis:(colors:(Count:%237EB26D))),P-3:(vis:(colors:(Count:%23D683CE)))))");

        wv.setWebViewClient(new WebViewClient() {
//        WebView view = (WebView) webView.findViewById(R.id.webView);
//
//        view.getSettings().setJavaScriptEnabled(true);
//       view.loadUrl("http://192.168.91.126:5601/app/kibana#/dashboard/922c6fe0-20d8-11e7-bcb9-b5b0748593aa?embed=true&_g=()&_a=(filters:!(),options:(darkTheme:!f),panels:!((col:7,id:f4c8c160-201c-11e7-a6d5-5b7a2c156bd0,panelIndex:1,row:1,size_x:6,size_y:7,type:visualization),(col:1,id:%271b847d80-2036-11e7-b54e-796ca9a883ed%27,panelIndex:2,row:1,size_x:6,size_y:3,type:visualization),(col:1,id:%277d4a44f0-1f55-11e7-965e-75c76c343bf1%27,panelIndex:3,row:4,size_x:6,size_y:3,type:visualization)),query:(query_string:(analyze_wildcard:!t,query:%27*%27)),title:multiple,uiState:(P-1:(spy:(mode:(fill:!f,name:!n)),vis:(colors:(Count:%237EB26D))),P-3:(vis:(colors:(Count:%23D683CE)))))");
//
//        view.setWebViewClient(new WebViewClient(){
            @Override
            public  boolean shouldOverrideUrlLoading(WebView view1,String url)
            {
                view1.loadUrl(url);
                return false;
            }
        });
        return rootView;
    }

}